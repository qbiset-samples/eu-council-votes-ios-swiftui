## European Council Voting Records
Author: Quentin BISET  
Version: 1.1.0  
Platform: iOS (universal)  
Design pattern: MVVM + Clean Architecture w/ SwiftUI  
Language: Swift 5  
Frameworks: SwInject for dependency injection

---
### What does the app do?
The app uses a [free web API ](http://api.epdb.eu/) to obtain the voting records of all proposals considered by the European Council, and displays them in a **SplitView**.

---
### How is the project organized?
##### Clean Architecture
The app makes use of 2 Swift packages on top of the main project:
- **Domain** defines protocols for the model and data repository, and contains a Swift-only UseCase class that handles non-UI operations and exposes relevant Publishers. This UseCase is meant to be injected into the main project's ViewModel.
- **ProposalsData** contains the actual implementation of the data repository. It is in charge of querying the web API, storing the data locally and making it available through Publishers. It is meant to be injected into **ProposalsSplitViewUseCase**. At this time the data is simply stored in memory, but a future implementation will use CoreData for permanent local storage.
- **EU Votes** is the main project, in charge of the app's UI (views and ViewModel). **SplitViewModel** is instantiated using SwInject to inject **ProposalsSplitViewUseCase** into it.

##### Retrieving and refreshing proposals data
When **SplitView** first appears, its associated **SplitViewModel** interrogates the web API using an instance of **ProposalsSplitViewUseCase**. This is a 3-step process:
1. [This webservice](http://api.epdb.eu/council/country/) is interrogated to retrieve the ID, name and voting weight of all EU countries.
2. [This webservice](http://api.epdb.eu/council/document/) is interrogated to retrieve all relevant data regarding the proposals the Council voted on.
3. The two sets of data are aggregated and kept locally in a **CurrentValueSubject**, so Publishers subscribing to the data set will be automatically updated.

When the query is underway, a loading view should be displayed by **ProposalDetailsView** if it is visible and we do not already have local data, or by **ProposalsListView** otherwise.

While the data is very unlikely to change, a refresh button is available on **ProposalsListView**'s navigation bar to interrogate the web API again without having to kill and restart the app.

##### Updating the UI
**SplitViewModel**, which contains an instance of **ProposalsSplitViewUseCase**, exposes Publishers to access the local data (after transformation to make it ready for display). **ProposalsListView** and **ProposalDetailsView** subscribe to said Publishers to update their @State view data variable, which will update the UI.

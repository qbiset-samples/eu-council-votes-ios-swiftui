//
//  IntegrationTests.swift
//  IntegrationTests
//
//  Created by Quentin Biset on 06/11/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes

final class IntegrationTests : XCTestCase {
    var injector: Injector!

    override func setUp() async throws {
        self.injector = Injector()
    }

    override func tearDown() async throws {
        self.injector = nil
    }

    func testInjectedSplitViewModel() async throws {
        let vm: SplitViewModel = injector.getSplitViewModel()

        let expectation = XCTestExpectation(description: "Expecting data received from web API, stored locally and transformed into ViewData by the ViewModel.")

        let cancellable = vm.listPublisher.sink(receiveCompletion: { completion in
            // Raise unexpected publisher error
            if case .failure(let error) = completion {
                XCTFail(error.localizedDescription)
            }
        }, receiveValue: { viewData in
            // Fulfill expectation if data is present; do not use an assert since the data will empty at first.
            if (viewData.proposalsSection.count > 0) {
                expectation.fulfill()
            }
        })

        // Start force fetch
        vm.refreshDataModel()

        await fulfillment(of: [expectation], timeout: 20)
        cancellable.cancel()
    }
}

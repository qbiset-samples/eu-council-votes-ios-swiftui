//
//  Country.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 09/01/2024.
//

import Foundation

/**
 This class is used to contain various data about the countries voting on the EU Council.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public protocol Country: Identifiable {
    // MARK: Properties
    /**
     An arbitrary identification number, received from the web API.
     */
    var id: Int { get }
    /**
     The name of the country, in English.
     */
    var name: String { get }
    /**
     Weight of the country's vote.
     */
    var voteWeight: Int { get }
}

// MARK: - Mocks
#if DEBUG
public struct MockCountry : Country {
    public let id: Int
    public let name: String
    public let voteWeight: Int
}

public struct MockCountries {
    public static let france = MockCountry(id: 1, name: "France", voteWeight: 1)
    public static let germany = MockCountry(id: 2, name: "Germany", voteWeight: 1)
    public static let italy = MockCountry(id: 3, name: "Italy", voteWeight: 1)
}
#endif

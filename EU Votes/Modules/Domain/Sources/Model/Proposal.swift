//
//  Proposal.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 09/01/2024.
//

import Foundation

/**
 This class represents one of the proposals the EU Council has voted on.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public protocol Proposal: Identifiable {
    // MARK: Properties
    /**
     An arbitrary identification number, received from the web API.
     */
    var id: Int { get }
    /**
     The full title of the proposal.
     */
    var title: String { get }
    /**
     The date the proposal was voted on.
     */
    var date: Date { get }
    /**
     An array of `Vote` objects, describing the various countries' vote.
     */
    var votes: [any Vote] { get }
}

// MARK: - Mocks
#if DEBUG
public struct MockProposal : Proposal {
    public let id: Int
    public let title: String
    public let date: Date
    public let votes: [any Vote]
}

public struct MockProposals {
    public static let proposal1 = MockProposal(id: 1, title: "Proposal ONE: Proposals usually have *very* long titles, so we will write a very long title in our mock data to get more accurate previews. They are really very long so we will write a little more nonsense here. And again, a little more.", date: Date(), votes: MockVotes.all)
    public static let proposal2 = MockProposal(id: 2, title: "Proposal TWO: Proposals usually have *very* long titles, so we will write a very long title in our mock data to get more accurate previews. They are really very long so we will write a little more nonsense here. And again, a little more.", date: Date(), votes: MockVotes.all)
    public static let proposal3 = MockProposal(id: 3, title: "Proposal THREE: Proposals usually have *very* long titles, so we will write a very long title in our mock data to get more accurate previews. They are really very long so we will write a little more nonsense here. And again, a little more.", date: Date(), votes: MockVotes.all)
    public static let all: [any Proposal] = [proposal1, proposal2, proposal3]
}
#endif

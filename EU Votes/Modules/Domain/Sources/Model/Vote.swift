//
//  Vote.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 09/01/2024.
//

import Foundation

/**
 This class represents a country's vote on a proposal.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public protocol Vote {
    // MARK: Properties
    /**
     The country casting the vote.
     */
    var country: any Country { get }
    /**
     The value of the vote.

     - Note: I cannot find a definitive list of possible values here, or we could have used an enum for this property.
     */
    var value: String { get }
}

// MARK: - Mocks
#if DEBUG
public struct MockVote : Vote {
    public let country: any Country
    public let value: String
}

public struct MockVotes {
    public static let france = MockVote(country: MockCountries.france, value: "For")
    public static let germany = MockVote(country: MockCountries.germany, value: "For")
    public static let italy = MockVote(country: MockCountries.italy, value: "Against")
    public static let all: [any Vote] = [france, germany, italy]
}
#endif

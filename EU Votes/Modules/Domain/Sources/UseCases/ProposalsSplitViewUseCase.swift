//
//  ProposalsSplitViewUseCase.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 15/01/2024.
//

import Foundation
import OSLog
import Combine
import Model
import Repositories

public class ProposalsSplitViewUseCase {
    // MARK: Status enum
    /** This will be used by the relevant view models to know if they should display a loading indicator or an error message. */
    public enum Status { case idle, fetching, error }

    // MARK: Constants
    private enum K {
        static let secondsInOneDay: Double = 86400
    }

    // MARK: Private properties
    private let repository: ProposalsRepository
    private let logger = Logger(subsystem: "com.qbiset.EU-Votes.UseCases", category: "ProposalsSplitViewUseCase")
    private var status = CurrentValueSubject<Status, Never>(.idle)

    // MARK: Initialisation
    public init(repository: ProposalsRepository) {
        self.repository = repository
    }

    // MARK: Publishers
    /** Returns a publisher broadcasting the status of the data fetching operation, if there is one. Used in the List View. */
    public func getStatusPublisher() -> AnyPublisher<ProposalsSplitViewUseCase.Status, Never> {
        return status.eraseToAnyPublisher()
    }

    /** Returns a publisher broadcasting the list of proposals available locally. Used in the List view. */
    public func getProposalsPublisher() -> AnyPublisher<[any Proposal], Never> {
        return repository.getProposalsPublisher()
    }

    /** Returns a publisher boardcasting the data of a particular proposal. Used in the Details view. */
    public func getProposalPublisher(withId id: Int) -> AnyPublisher<Result<any Proposal, ProposalsRepositoryError>, Never> {
        return repository.getProposalPublisher(withId: id)
    }

    // MARK: Fetching data
    /** Fetches all necessary data from the web API if it has been at least a day since the last successful fetch (or if the data has never been fetched before), and stores it locally. */
    public func fetchDataIfNeeded() async {
        let lastFetchTimestamp = UsefulUserDefaults.getLastSuccessfulFetchTimestamp()
        guard (repository.isStorageEmpty ||
               lastFetchTimestamp == 0 ||
               Date.timeIntervalSinceReferenceDate > (lastFetchTimestamp + K.secondsInOneDay))
        else { return }

        await fetchData()
    }

    /** Fetches all necessary data from the web API and stores it locally. */
    public func fetchData() async {
        guard status.value != .fetching else { return }

        status.value = .fetching
        let result = await repository.fetchData()
        switch result {
        case .success:
            UsefulUserDefaults.storeSuccessfulFetchTimestamp()
            status.value = .idle
        case .failure(let error):
            logger.error("Failed to load data: \(error.localizedDescription)")
            status.value = .error
            break
        }
    }
}

//
//  ProposalsRepository.swift
//  EU Votes - Domain
//
//  Created by Quentin Biset on 15/01/2024.
//

import Foundation
import Combine
import Model

/**
 A repository handling all data related to proposals voted on by the EU Council.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public protocol ProposalsRepository {
    // MARK: Fetching data
    /** Fetches all proposals, countries and votes data and stores it locally. */
    func fetchData() async -> Result<Void, ProposalsRepositoryError>

    // MARK: Accessing data
    /** Tests if there are proposals in the local storage. */
    var isStorageEmpty: Bool { get }
    /** Retrieves a Publisher of all Proposal items in storage. */
    func getProposalsPublisher() -> AnyPublisher<[any Proposal], Never>
    /** Retrieves all data concerning a Proposal from storage and returns it through a Publisher. */
    func getProposalPublisher(withId id: Int) -> AnyPublisher<Result<any Proposal, ProposalsRepositoryError>, Never>
}

// MARK: -
/**
 An enum of possible errors thrown by the repository's publishers.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public enum ProposalsRepositoryError: Error {
    /**
     Any kind of error raised during the fetch operation (connection failure, decoding error, etc...).

     We do not really care about the underlying issue outside of integration tests since we'll just abort the operation and rely on the local storage.
     */
    case fetchError
    /** When calling `getProposalPublisher(withId:)`, the returned publisher may throw this error if no Proposal could be found with the requested id. */
    case proposalNotFound
    /** An unexpected error. */
    case unknown
}

// MARK: - Mocks
#if DEBUG
public class MockProposalsRepository : ProposalsRepository {

    public enum FetchSimulation {
        /** `fetchData()` will simulate success after a delay. The delay can be 0. */
        case success(delay: UInt64)
        /** `fetchData()` will simulate error after a delay. The delay can be 0. */
        case failure(delay: UInt64)
    }

    private let proposals: [any Proposal]
    private let fetchSimulation: FetchSimulation

    public init(proposals: [any Proposal] = MockProposals.all, fetchSimulation: FetchSimulation = .success(delay: 2)) {
        self.proposals = proposals
        self.fetchSimulation = fetchSimulation
    }

    public var isStorageEmpty: Bool { proposals.isEmpty }

    public func getProposalsPublisher() -> AnyPublisher<[any Proposal], Never> {
        return Just(proposals).eraseToAnyPublisher()
    }

    public func getProposalPublisher(withId id: Int) -> AnyPublisher<Result<any Proposal, ProposalsRepositoryError>, Never> {
        let result: Result<any Proposal, ProposalsRepositoryError>
        if let proposal = proposals.first(where: { $0.id == id }) {
            result = .success(proposal)
        } else {
            result = .failure(.proposalNotFound)
        }
        return Just(result).eraseToAnyPublisher()
    }

    public func fetchData() async -> Result<Void, ProposalsRepositoryError> {
        switch fetchSimulation {
        case .success(let delay):
            try? await Task.sleep(nanoseconds: delay * NSEC_PER_SEC)
            return .success(())
        case .failure(let delay):
            try? await Task.sleep(nanoseconds: delay * NSEC_PER_SEC)
            return .failure(.fetchError)
        }
    }
}
#endif

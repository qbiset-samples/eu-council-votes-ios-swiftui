import XCTest
import Combine
@testable import Model
@testable import Repositories
@testable import UseCases

final class SplitViewUseCaseTests: XCTestCase {
    // Constants
    private struct K {
        static let expectationTimeout: TimeInterval = 10
    }

    /** 3 proposals, fetch succeeds immediately. */
    func testUseCaseWithDefaultMock() async throws {
        let mockRepo = MockProposalsRepository()
        let useCase = ProposalsSplitViewUseCase(repository: mockRepo)

        var cancellations: [AnyCancellable] = []

        // 1. Status with fetchData()
        let idleExpectation = XCTestExpectation(description: "Published status should be .idle at least once.")
        let fetchingExpectation = XCTestExpectation(description: "Published status should be .fetching at least once.")
        useCase.getStatusPublisher().sink { status in
            switch status {
            case .idle:
                idleExpectation.fulfill()
            case .fetching:
                fetchingExpectation.fulfill()
            case .error:
                XCTFail("Status should never be .error in this mock UseCase.")
            }
        }.store(in: &cancellations)

        await useCase.fetchData()

        // 2. Proposals list
        let listExpectation = XCTestExpectation(description: "Published list of proposals should contain expected number of proposals.")
        useCase.getProposalsPublisher().sink { proposals in
            XCTAssertEqual(proposals.count, 3, "Proposals should contain 3 proposals.")
            listExpectation.fulfill()
        }.store(in: &cancellations)

        // 3. Proposal details
        let proposalExpectation = XCTestExpectation(description: "Publisher should show proposal since the ID exists in the default mock repository.")
        let mockProposal = MockProposals.proposal2 // Is part of the mock repository with default parameters.
        useCase.getProposalPublisher(withId: mockProposal.id).sink { result in
            switch result {
            case .success(let proposal):
                XCTAssertEqual(proposal.id, mockProposal.id)
                XCTAssertEqual(proposal.date, mockProposal.date)
                XCTAssertEqual(proposal.title, mockProposal.title)
            case .failure(let error):
                XCTFail("The publisher should be able to return a success with the correct proposal. Error: \(error.localizedDescription)")
            }
            proposalExpectation.fulfill()
        }.store(in: &cancellations)

        // 4. Proposal details with wrong ID
        let noProposalExpectation = XCTestExpectation(description: "Publisher should NOT show proposal since the ID does not exist in the default mock repository.")
        useCase.getProposalPublisher(withId: 42).sink { result in
            if case .success(_) = result {
                XCTFail("The result should be a failure.")
            }
            noProposalExpectation.fulfill()
        }.store(in: &cancellations)

        await fulfillment(
            of: [idleExpectation, fetchingExpectation, listExpectation, proposalExpectation, noProposalExpectation],
            timeout: K.expectationTimeout
        )
        cancellations.forEach { $0.cancel() }
    }

    /** No proposals, fetch fails immediately. */
    func testUseCaseWithErrorMock() async throws {
        let mockRepo = MockProposalsRepository(proposals: [], fetchSimulation: .failure(delay: 2))
        let useCase = ProposalsSplitViewUseCase(repository: mockRepo)

        var cancellations: [AnyCancellable] = []

        // 1. Status with fetchData()
        let fetchingExpectation = XCTestExpectation(description: "Published status should be .idle at least once.")
        let errorExpectation = XCTestExpectation(description: "Published status should be .error at least once.")
        useCase.getStatusPublisher().sink { status in
            switch status {
            case .idle:
                break
            case .fetching:
                fetchingExpectation.fulfill()
            case .error:
                errorExpectation.fulfill()
            }
        }.store(in: &cancellations)

        await useCase.fetchData()

        // 2. Proposals list
        let listExpectation = XCTestExpectation(description: "Published list of proposals should be empty.")
        useCase.getProposalsPublisher().sink { proposals in
            XCTAssertTrue(proposals.isEmpty, "There should be no proposals.")
            listExpectation.fulfill()
        }.store(in: &cancellations)

        // 3. Proposal details
        let proposalExpectation = XCTestExpectation(description: "Publisher should NOT show proposal since the mock repository should be empty.")
        let mockProposal = MockProposals.proposal2
        useCase.getProposalPublisher(withId: mockProposal.id).sink { result in
            print(result)
            if case .success(let proposal) = result {
                XCTFail("There should be no proposals in the mock repository but this proposal was found: \(proposal)")
            }
            proposalExpectation.fulfill()
        }.store(in: &cancellations)

        await fulfillment(
            of: [fetchingExpectation, errorExpectation, listExpectation, proposalExpectation],
            timeout: K.expectationTimeout
        )
        cancellations.forEach { $0.cancel() }
    }
}

//
//  WebApiError.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 24/01/2024.
//

import Foundation

/**
 A utility class used to create Error objects used in the web API data source.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
class WebApiErrorFactory {
    // MARK: Constants
    private enum K {
        static let errorDomain: String = "WebApiDataSource"
        static let badUrlErrorDescriptionKey: String = "WebApiDataSource.error.badUrl"
        static let urlSessionErrorDescriptionKey: String = "WebApiDataSource.error.urlSession"
        static let noDataErrorDescriptionKey: String = "WebApiDataSource.error.noData"
        static let jsonDecodingErrorDescriptionKey: String = "WebApiDataSource.error.jsonDecoding"
    }

    // MARK: Initialisation
    private init() {}

    // MARK: Convenience functions
    /**
     A convenience function used to quickly generate a NSError object for one of our Service classes.

     - Parameter service: Identify which Service class needs the Error object. This is used to choose the correct  domain string.
     - Parameter type: Identify the type of error we want to create. This is used to choose the correct localized description string.
     - Parameter error: If applicable, the Error object that was raised by the system and that we are trying to propagate.

     - Returns: A NSError object with a custom domain name and localized description, and the underlying error if applicable.
     */
    class func error(ofType type: WebApiErrorFactory.ErrorType, withUnderlyingError error: Error? = nil) -> Error {

        let code: Int = type.rawValue
        var localizedDescription: String

        switch type {
        case .badURL:
            localizedDescription = NSLocalizedString(K.badUrlErrorDescriptionKey, bundle: Bundle.module, comment: "")
        case .urlSession:
            localizedDescription = NSLocalizedString(K.urlSessionErrorDescriptionKey, bundle: Bundle.module, comment: "")
        case .noData:
            localizedDescription = NSLocalizedString(K.noDataErrorDescriptionKey, bundle: Bundle.module, comment: "")
        case .jsonDecoding:
            localizedDescription = NSLocalizedString(K.jsonDecodingErrorDescriptionKey, bundle: Bundle.module, comment: "")
        }

        var userInfo: [String:Any] = [NSLocalizedDescriptionKey: localizedDescription]
        if let underlyingError: Error = error {
            userInfo[NSUnderlyingErrorKey] = underlyingError
        }

        return NSError(domain: K.errorDomain, code: code, userInfo: userInfo)
    }
    
    // MARK: Enumerations
    /**
     An enumeration used to identify the type of error we are trying to raise.
     */
    enum ErrorType: Int {
        /**
         An URL object could not be instanciated.
         */
        case badURL = -1
        /**
         The URLSessionDataTask returned an error.
         */
        case urlSession = -2
        /**
         The URLSessionDataTask returned no data despite succeeding.
         */
        case noData = -3
        /**
         The received JSON data could not be decoded.
         */
        case jsonDecoding = -4
    }
}

//
//  WebApiDataSource.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 18/01/2024.
//

import Foundation
import DomainModel
import OSLog

/**
 The data source used to get countries/proposals data from the free web API `api.epdb.eu`.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
public class WebApiDataSource {
    // MARK: Constants
    private enum K {
        /**
         The URL to obtain the list of countries and their associated data.
         */
        static let countriesUrlString: String = "http://api.epdb.eu/council/country/"
        /**
         The URL to obtain the list of proposals voted on by the EU Council..
         */
        static let proposalsUrlString: String = "http://api.epdb.eu/council/document/"

        static let numberOfRetries: Int = 1
    }

    private let logger = Logger(subsystem: "com.qbiset.EU-Votes.ProposalsData", category: "WebApiDataSource")

    // MARK: Initialisation
    public init() {}

    // MARK: Computed properties
    /**
     An ephemeral URLSession, used to query the API.
     */
    private var urlSession: URLSession { URLSession(configuration: URLSessionConfiguration.ephemeral) }

    // MARK: Functions
    public func getCountries() async throws -> [any Country] {
        logger.debug("Fetching countries data from remote API.")

        guard let url: URL = URL(string: K.countriesUrlString) else {
            logger.error("Failed to resolve URL \(K.countriesUrlString)")
            throw WebApiErrorFactory.error(ofType: .badURL)
        }

        let data: Data = try await fetchData(url: url)
        return try WebApiParsing.parseCountriesData(data, logger: logger)
    }

    public func getProposals(withCountries countries: any Collection<Country>) async throws -> [any Proposal] {
        logger.debug("Fetching proposals data from remote API.")

        guard let url: URL = URL(string: K.proposalsUrlString) else {
            logger.error("Failed to resolve URL \(K.proposalsUrlString)")
            throw WebApiErrorFactory.error(ofType: .badURL)
        }

        let data: Data = try await fetchData(url: url)
        let rawProposals: [RawProposal] = try WebApiParsing.parseProposalsData(data, logger: logger)

        return try rawProposals.map {
            try WebApiProposal.build(fromRawProposal: $0, withPossibleCountries: countries)
        }
    }

    // MARK: Private functions
    private func fetchData(url: URL) async throws -> Data {
        let data: Data

        do {
            let urlRequest: URLRequest = URLRequest(url: url)
            data = try await urlSession.data(for: urlRequest).0
        } catch {
            logger.error("Failed to fetch data: \(error.localizedDescription)")
            throw WebApiErrorFactory.error(ofType: .urlSession, withUnderlyingError: error)
        }

        guard !data.isEmpty else {
            logger.error("Fetched data is unexpectedly empty.")
            throw WebApiErrorFactory.error(ofType: .noData)
        }

        return data
    }
}

//
//  StorageDataSource.swift
//  EU Votes - ProposalsData
//
//  Created by Quentin Biset on 18/01/2024.
//

import Foundation
import Combine
import DomainModel

/**
 The data source used to get countries/proposals data from local storage (plain memory for now, Core Data later).
 */
public class StorageDataSource {
    // MARK: Properties
    public private(set) var countries: [any Country]
    public private(set) var proposals: CurrentValueSubject<[any Proposal], Never>

    // MARK: Initialisation
    public init() {
        countries = []
        proposals = CurrentValueSubject([])
    }

    // MARK: Computed properties
    public var isEmpty: Bool { return proposals.value.isEmpty }

    // MARK: Save functions
    public func saveCountries(_ countries: [any Country]) {
        self.countries = countries
    }

    public func saveProposals(_ proposals: [any Proposal]) {
        self.proposals.value = proposals
    }
}

// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ProposalsData",
    platforms: [
        // The minimum versions are chosen for their support of the Identifiable protocol.
        .macOS(.v11),
        .macCatalyst(.v14),
        .iOS(.v14),
        .tvOS(.v14),
        .watchOS(.v7),
        .visionOS(.v1),
    ],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "ProposalsData",
            targets: ["ProposalsDataRepositories"]),
    ],
    dependencies: [
        .package(path: "../Domain")
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "StorageDataSource",
            dependencies: [.product(name: "Domain", package: "Domain")]
        ),
        .target(
            name: "WebApiDataSource",
            dependencies: [.product(name: "Domain", package: "Domain")],
            resources: [.process("Resources")]
        ),
        .target(
            name: "ProposalsDataRepositories",
            dependencies: [
                .target(name: "StorageDataSource"),
                .target(name: "WebApiDataSource"),
                .product(
                    name: "Domain",
                    package: "Domain",
                    moduleAliases: [
                        "Model":"DomainModel",
                        "Repositories":"DomainRepositories"
                    ]
                ),
            ]
        ),
        .testTarget(
            name: "WebApiTests",
            dependencies: ["WebApiDataSource"],
            resources: [.process("Resources")]
        ),
        .testTarget(
            name: "IntegrationTests",
            dependencies: ["ProposalsDataRepositories"]
        )
    ]
)

//
//  SplitViewModelTests.swift
//  UnitTests
//
//  Created by Quentin Biset on 06/11/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import XCTest
@testable import EU_Council_Votes
@testable import UseCases
import DomainModel
import DomainRepositories

final class SplitViewModelTests : XCTestCase {
    // Constants
    private struct K {
        static let expectationTimeout: TimeInterval = 10
    }


    var vm: SplitViewModel!

    override func setUp() async throws {
        let repo = MockProposalsRepository(proposals: MockProposals.all, fetchSimulation: .failure(delay: 2))
        let useCase = ProposalsSplitViewUseCase(repository: repo)
        self.vm = SplitViewModel(useCase: useCase)
    }

    override func tearDown() async throws {
        self.vm = nil
    }

    /**
     Tests the publisher of an instance of SplitViewModel responsible for broadcasting the ViewData of the list of proposals.

     We use a mock UseCase containing 3 proposals and the "fetching" status: we expect the broadcasted ViewData to contain at least one special cell data and 3 proposal cells data.
     */
    func testListPublisher() async throws {
        let expectation = XCTestExpectation(description: "Expecting data received from web API, stored locally and transformed into ViewData by the ViewModel.")

        let cancellable = vm.listPublisher.sink(receiveCompletion: { completion in
            // Raise unexpected publisher error
            if case .failure(let error) = completion {
                XCTFail(error.localizedDescription)
            }
        }, receiveValue: { viewData in
            XCTAssertEqual(viewData.specialSection.count, 1, "There should be one entry in the special section (error status).")
            XCTAssertEqual(viewData.proposalsSection.count, 3, "There should be 3 entries in the proposals section.")
            expectation.fulfill()
        })

        await fulfillment(of: [expectation], timeout: K.expectationTimeout)
        cancellable.cancel()
    }

    /**
     Tests the publisher of an instance of SplitViewModel responsible for broadcasting the ViewData of the details of a particular proposal.

     We use a mock UseCase containing 3 proposals and the "fetching" status: we expect the broadcasted ViewData to contain the details of the second proposal.
     */
    func testDetailsPublisher() async throws {
        let mockProposal = MockProposals.proposal2

        let expectation = XCTestExpectation(description: "Expecting data received from web API, stored locally and transformed into ViewData by the ViewModel.")

        let cancellable = vm.getDetailsPublisher(proposalId: mockProposal.id).sink(receiveCompletion: { completion in
            // Raise unexpected publisher error
            if case .failure(let error) = completion {
                XCTFail(error.localizedDescription)
            }
        }, receiveValue: { state in
            switch state {
            case .normal(let viewData):
                XCTAssertEqual(viewData.title, mockProposal.title)
            case .status(let viewData):
                XCTFail("Wrong state broadcasted: \(viewData)")
            }

            expectation.fulfill()
        })

        await fulfillment(of: [expectation], timeout: K.expectationTimeout)
        cancellable.cancel()
    }

}

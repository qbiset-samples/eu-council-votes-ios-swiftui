//
//  ProposalDetailsView.swift
//  EU Votes
//
//  Created by Quentin Biset on 22/06/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import SwiftUI

/**
 The detail view displays the data of one of the proposals presented to the EU Council.

 If the main service is retrieving data for the first time, a loading view will be displayed until the service finished. If the service failed, an error message will be displayed.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalDetailsView: View {
    // MARK: Properties
    let viewModel: SplitViewModel
    let proposalId: Int?
    @State var state: SplitViewModel.DetailsState = SplitViewModel.DetailsState.status(DetailsStatusViewDataFactory.buildEmptyStatus())

    // MARK: UI
    var body: some View {
        Group {
            switch state {
            case .normal(let viewData):
                ProposalView(viewData: viewData)
            case .status(let viewData):
                DetailsStatusView(viewData: viewData)
            }
        }
        .navigationBarTitle("", displayMode: .inline)
        .onReceive(viewModel.getDetailsPublisher(proposalId: proposalId)) { self.state = $0 }
    }
}

// MARK: -
/**
 This view shows a status message and an activity indicator if necessary.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct DetailsStatusView: View {
    // MARK: UI
    let viewData: DetailsStatusViewData

    var body: some View {
        VStack {
            Text(viewData.title)
                .multilineTextAlignment(.center)
                .padding()
            if viewData.shouldShowActivityIndicator {
                ActivityIndicatorView(isAnimating: .constant(true), style: .large)
            }
        }
    }
}

// Previews ---------------------
#Preview("IdlePreview") { ProposalDetailsView(viewModel: MockSplitViewModels.defaultMock, proposalId: 1) }
#Preview("NotSelectedPreview") { ProposalDetailsView(viewModel: MockSplitViewModels.defaultMock, proposalId: nil) }
#Preview("NotFoundPreview") { ProposalDetailsView(viewModel: MockSplitViewModels.defaultMock, proposalId: 1) }

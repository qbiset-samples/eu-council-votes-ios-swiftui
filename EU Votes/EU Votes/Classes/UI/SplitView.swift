//
//  ContentView.swift
//  EU Votes
//
//  Created by Quentin Biset on 21/06/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import SwiftUI

/**
 The root view of our application, representing what would have been a UISplitViewController in UIKit.
 
 At launch, the ViewModel will retrieve the voting data from a web API.
 
 - SeeAlso: `ProposalsListView`, `ProposalDetailsView`
 
 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct SplitView: View {
   // MARK: Properties
    let viewModel: SplitViewModel

   // MARK: UI
    var body: some View {
        NavigationView {
         ProposalsListView(viewModel: viewModel)
         ProposalDetailsView(viewModel: viewModel, proposalId: nil)
        }.navigationViewStyle(DoubleColumnNavigationViewStyle())
         .onAppear {
            self.viewModel.fetchDataIfNeeded()
      }
    }
}

// Previews ---------------------
#Preview("IdlePreview") { SplitView(viewModel: MockSplitViewModels.defaultMock) }
#Preview("ErrorPreview") { SplitView(viewModel: MockSplitViewModels.errorMock) }
#Preview("RealVmPreview") { SplitView(viewModel: Injector().getSplitViewModel())}

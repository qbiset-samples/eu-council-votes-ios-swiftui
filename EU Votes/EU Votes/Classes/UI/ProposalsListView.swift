//
//  ProposalsListView.swift
//  EU Votes
//
//  Created by Quentin Biset on 22/06/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import SwiftUI

/**
 The master view shows a list of proposals presented to the EU Council.

 If the main service is retrieving data, a loading banner will appear at the top of the list. Additionally, an error banner will be displayed if the service failed. Note that those two behaviours are disabled during the very first request if the detail view is shown, because the detail view will already be showing a loading view.

 Tapping on a proposal will update the detail view (or navigate to the detail view if it is not already displayed, presumably because the user is on a small device).

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalsListView: View {
    // MARK: Constants
    private enum K {
        static let navBarTitleKey: String = "list.navigationBar.title"
        static let navBarTitleComment: String = "Title of the navigation bar."

        static let refreshIconSystemName: String = "arrow.clockwise"
    }

    // MARK: Properties
    let viewModel: ListViewModel
    @State private var viewData = ListViewData()

    // MARK: UI
    var body: some View {
        List {
            ForEach(viewData.specialSection, id: \.id) { cellData in
                switch cellData {
                case is LoadingCellData:
                    ProposalsListLoadingCell(cellData: cellData as! LoadingCellData)
                case is ErrorCellData:
                    ProposalsListFailureCell(cellData: cellData as! ErrorCellData)
                default: // Should never happen.
                    EmptyView()
                }
            }

            ForEach(viewData.proposalsSection, id: \.identifier) { cellData in
                NavigationLink(destination: ProposalDetailsView(viewModel: viewModel, proposalId: cellData.identifier)) {
                    ProposalsListProposalCell(cellData: cellData)
                }
                .isDetailLink(true)
            }
        }
        .animation(.default, value: viewData.specialSection.count)
        .animation(.default, value: viewData.proposalsSection.count)
        .navigationBarTitle(NSLocalizedString(K.navBarTitleKey, comment: K.navBarTitleComment))
        .navigationBarItems(
            trailing: Button(
                action: {
                    if (viewData.shouldEnableRefresh) {
                        self.viewModel.refreshDataModel()
                    }
                }
            ) { Image(systemName: K.refreshIconSystemName) }
        ).onReceive(viewModel.listPublisher) { self.viewData = $0 }
    }
}

// MARK: -
/**
 This view shows a loading message and an activity indicator to inform the user that the app is retrieving the data from a webservice.

 It will be shown at the top of the *List* in `ProposalsListView` if needed.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalsListLoadingCell: View {
    // MARK: Properties
    private let message: String

    // MARK: Initialization
    init(cellData: LoadingCellData) {
        self.message = cellData.text
    }

    // MARK: UI
    var body: some View {
        HStack {
            Text(message).padding(.trailing, 10.0)
            ActivityIndicatorView(isAnimating: .constant(true), style: .medium)
        }
        .padding(EdgeInsets(top: 8.0, leading: 2.0, bottom: 8.0, trailing: 2.0))
    }
}

// MARK: -
/**
 This view shows an error message to inform the user that the app could not download or refresh the data.

 It will be shown at the top of the *List* in `ProposalsListView` if needed.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalsListFailureCell: View {
    // MARK: Properties
    private let message: String

    // MARK: Initialization
    init(cellData: ErrorCellData) {
        self.message = cellData.text
    }

    // MARK: UI
    var body: some View {
        Group {
            Text(message).multilineTextAlignment(.center)
                .frame(maxWidth: .infinity, alignment: .center)
        }.padding(EdgeInsets(top: 8.0, leading: 2.0, bottom: 8.0, trailing: 2.0))
    }
}

// MARK: -
/**
 This view displays the date and part of the title of a proposal.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalsListProposalCell: View {
    // MARK: Instance properties
    /**
     The proposal whose data will be displayed.
     */
    let cellData: ProposalCellData

    // MARK: UI
    var body: some View {
        VStack(alignment: .leading) {
            Text(cellData.dateString)
                .font(.headline)
                .padding([.top, .bottom], 8.0)
            Text(cellData.title)
                .font(.body)
                .lineLimit(3)
                .padding(.bottom, 8.0)
        }
    }
}

// Previews ---------------------
#Preview("IdlePreview") { ProposalsListView(viewModel: MockSplitViewModels.defaultMock) }
#Preview("ErrorPreview") { ProposalsListView(viewModel: MockSplitViewModels.errorMock) }

//
//  ProposalView.swift
//  EU Votes
//
//  Created by Quentin Biset on 30/06/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import SwiftUI

/**
 This view displays the date, title and voting records of a proposal presented to the EU Council.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalView: View {
    // MARK: Properties
    /**
     The proposal whose data will be displayed.
     */
    let viewData: DetailsViewData

    // MARK: UI
    var body: some View {
        ScrollView(.vertical) {
            VStack(alignment: .leading) {
                ProposalHeaderView(formattedDate: viewData.dateString, title: viewData.title)
                Divider()
                ProposalVotesView(votes: viewData.votes)
            }
            .padding(.top, 10.0)
            .padding(.bottom, 5.0)
        }
    }
}

// MARK: -
/**
 This view displays the date and title of a proposal presented to the EU Council.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalHeaderView: View {
    // MARK: Instance properties
    let formattedDate: String
    let title: String

    // MARK: UI
    var body: some View {
        VStack(alignment: .leading) {
            Text(formattedDate)
                .font(.subheadline)
                .padding(.bottom, 10.0)
            Text(title)
                .font(.headline)
                .lineLimit(nil)
                .padding(.bottom, 5.0)
        }
        .padding([.leading, .trailing], 15.0)
    }
}

// MARK: -
/**
 This view displays the voting records of a proposal presented to the EU Council.

 - Author: Quentin BISET (qbiset@gmail.com)
 */
struct ProposalVotesView: View {
    // MARK: Instance properties
    let votes: [VoteCellViewData]

    // MARK: UI
    var body: some View {
        Group {
            ForEach(votes, id: \.countryName) { vote in
                VStack(alignment: .leading) {
                    HStack {
                        Text(vote.countryName)
                        Spacer()
                        Text(vote.vote)
                    }
                    .padding([.leading, .trailing], 15.0)

                    Divider()
                }
            }
        }
    }
}

#if DEBUG
// MARK: -
// TODO: remake previews after refactoring
/*struct ProposalView_Previews: PreviewProvider {
    static var previews: some View {
        ProposalView(proposal: MockProposals.proposal1)
    }
}*/
#endif

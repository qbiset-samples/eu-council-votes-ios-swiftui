//
//  SplitViewModel.swift
//  EU Votes
//
//  Created by Quentin Biset on 03/03/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import Foundation
import Combine
import UseCases
import DomainModel
import DomainRepositories

protocol ListViewModel : SplitViewModel {
    var listPublisher: AnyPublisher<ListViewData, Never> { get }

    func fetchDataIfNeeded()
    func refreshDataModel()
}

protocol DetailsViewModel : SplitViewModel {
    func getDetailsPublisher(proposalId: Int?) -> AnyPublisher<SplitViewModel.DetailsState, Never>
}

/**
 The source of truth for `SplitView` and its subviews.
 
 It uses an instance of `ProposalsSplitViewUseCase` to handle the retrieval and storage of the data, and can publish the states of the split view's subviews. Since we use publishers directly, `SplitViewModel` does not need to be an ObservableObject.
 
 - Author: Quentin BISET (qbiset@gmail.com)
 */
class SplitViewModel {

    // MARK: Nested enums
    enum DetailsState {
        /** Show a proposal. */
        case normal(DetailsViewData)
        /** Show a status update. */
        case status(DetailsStatusViewData)
    }

    // MARK: Private properties
    private let useCase: ProposalsSplitViewUseCase

    // MARK: Public properties
    var areDetailsShowing: Bool = false
    var selectedProposalId: Int? = nil

    // MARK: Initialisation
    init(useCase: ProposalsSplitViewUseCase) {
        self.useCase = useCase
    }
}

// MARK: - For List
extension SplitViewModel : ListViewModel {
    // MARK: Publisher
    var listPublisher: AnyPublisher<ListViewData, Never> {
        return useCase.getStatusPublisher()
            .combineLatest(useCase.getProposalsPublisher()) { [weak self] (status, proposals) in
                guard let self = self else { return ListViewData() }

                let shouldEnableRefresh = (status != .fetching)
                let specialSectionViewData = self.buildListSpecialSection(status: status, areProposalsEmpty: proposals.isEmpty)
                // Proposals should already be sorted chronologically
                let proposalsSectionViewData = proposals.map { ProposalCellData(proposal: $0) }

                return ListViewData(shouldEnableRefresh: shouldEnableRefresh,
                                             specialSection: specialSectionViewData,
                                             proposalsSection: proposalsSectionViewData)
            }.subscribe(on: DispatchQueue.global(qos: .userInitiated))
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }

    private func buildListSpecialSection(
        status: ProposalsSplitViewUseCase.Status,
        areProposalsEmpty: Bool
    ) -> [any SpecialCellData] {
        // We do not show any special cell no matter the status if the details are visible and no proposal has been selected.
        guard !(areDetailsShowing && selectedProposalId == nil) else { return [] }

        switch status {
        case .idle:
            return []
        case .fetching:
            return [LoadingCellData()]
        case .error:
            return [ErrorCellData(areProposalsEmpty: areProposalsEmpty)]
        }
    }

    // MARK: Fetching data
    /** Asks the model to fetch data from the web API if the local storage is empty or if enough time has passed since the last fetch. */
    func fetchDataIfNeeded() {
        Task(priority: .userInitiated) {
            await useCase.fetchDataIfNeeded()
        }
    }

    /** Asks the model to refresh its data by querying the web API again. */
    func refreshDataModel() {
        Task(priority: .userInitiated) {
            await useCase.fetchData()
        }
    }
}

// MARK: - For Details
extension SplitViewModel : DetailsViewModel {
    /**
     A publisher to send states to `ProposalDetailsView`.
     */
    func getDetailsPublisher(proposalId: Int?) -> AnyPublisher<DetailsState, Never> {
        if let proposalId: Int = proposalId {
            return transformProposalPublisher(useCase: useCase, proposalId: proposalId)
        } else {
            return transformStatusPublisher(useCase: useCase)
        }
    }

    // MARK: Proposal
    /**
     Transforms the result of `ProposalsSplitViewUseCase.getProposalPublisher(withId:)` into a relevant state for `ProposalDetailsViewController`.
     */
    private func transformProposalPublisher(
        useCase: ProposalsSplitViewUseCase,
        proposalId: Int
    ) -> AnyPublisher<DetailsState, Never> {
        return useCase.getProposalPublisher(withId: proposalId).map { result in
            switch result {
            case .success(let proposal):
                let viewData = DetailsViewData(proposal: proposal)
                return DetailsState.normal(viewData)
            case .failure:
                return DetailsState.status(DetailsStatusViewDataFactory.buildProposalNotFoundStatus())
            }
        }.subscribe(on: DispatchQueue.global(qos: .userInitiated))
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }

    // MARK: Status
    /**
     Transforms the result of `ProposalsSplitViewUseCase.getStatusPublisher()` into a relevant state for `ProposalDetailsViewController`.

     This should only be useful if no proposal has been selected (typically when the app start with both panes visible, before the user has selected a proposal).
     */
    private func transformStatusPublisher(useCase: ProposalsSplitViewUseCase) -> AnyPublisher<DetailsState, Never> {
        return useCase.getStatusPublisher().map { status in
            switch status {
            case .idle:
                return DetailsState.status(DetailsStatusViewDataFactory.buildEmptyStatus())
            case .fetching:
                return DetailsState.status(DetailsStatusViewDataFactory.buildLoadingStatus())
            case .error:
                return DetailsState.status(DetailsStatusViewDataFactory.buildLoadingFailedStatus())
            }
        }.subscribe(on: DispatchQueue.global(qos: .userInitiated))
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}

// MARK: - Mocks
#if DEBUG
struct MockSplitViewModels {
    static let defaultMock = SplitViewModel(
        useCase: ProposalsSplitViewUseCase(repository: MockProposalsRepository(fetchSimulation: .success(delay: 5)))
    )
    static let errorMock = SplitViewModel(
        useCase: ProposalsSplitViewUseCase(repository: MockProposalsRepository(fetchSimulation: .failure(delay: 5)))
    )
}
#endif

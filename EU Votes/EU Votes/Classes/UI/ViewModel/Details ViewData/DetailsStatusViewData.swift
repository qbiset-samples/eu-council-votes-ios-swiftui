//
//  DetailsStatusViewData.swift
//  EU Votes
//
//  Created by Quentin Biset on 20/07/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import Foundation

// MARK: DetailsStatusViewData
struct DetailsStatusViewData {
    // MARK: Properties
    /** The text that will be displayed by the loading view. */
    let title: String
    /** Whether the loading view should show an UIActivityIndicatorView or not. */
    let shouldShowActivityIndicator: Bool
}

// MARK: - Factory
final class DetailsStatusViewDataFactory {
    /** The user has not selected a proposal yet. */
    class func buildEmptyStatus() -> DetailsStatusViewData {
        let title = NSLocalizedString("details.status.empty", comment: "Shown when the user has yet to select a proposal")
        return DetailsStatusViewData(title: title, shouldShowActivityIndicator: false)
    }
    /** The view model (and the UseCase it encapsulates) is fetching data from the web API. */
    class func buildLoadingStatus() -> DetailsStatusViewData {
        let title = NSLocalizedString("details.status.loading", comment: "Shown while remote data is being fetched")
        return DetailsStatusViewData(title: title, shouldShowActivityIndicator: true)
    }
    /** The view model (and the UseCase it encapsulates) failed to fetch data from the web API. */
    class func buildLoadingFailedStatus() -> DetailsStatusViewData {
        let title = NSLocalizedString("details.status.loading.failure", comment: "Shown when fetching data from the web API failed")
        return DetailsStatusViewData(title: title, shouldShowActivityIndicator: false)
    }
    /** A proposal was selected but its data could somehow not be found based on the identifier passed to `ProposalsSplitViewModel`. */
    class func buildProposalNotFoundStatus() -> DetailsStatusViewData {
        let title: String = NSLocalizedString("details.status.proposalNotFound", comment: "Shown when the model failed to retrieve a proposal based on its ID")
        return DetailsStatusViewData(title: title, shouldShowActivityIndicator: false)
    }

    private init() {}
}

//
//  VoteCellViewData.swift
//  EU Votes
//
//  Created by Quentin Biset on 20/07/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import Foundation
import DomainModel

/**
 This class will be the source of truth for a `ProposalVotesView`.
 
 It holds the country name and formatted value of a `Vote`.
 
 - Author: Quentin BISET (qbiset@gmail.com)
 */
class VoteCellViewData {
   // MARK: Properties
   /**
    The country whose vote will be displayed.
    */
   var countryName: String
   /**
    The value of the vote.
    */
   var vote: String
   
   // MARK: Initialization
   init(vote: any Vote) {
      self.countryName = vote.country.name
      self.vote = vote.value
   }
   
}

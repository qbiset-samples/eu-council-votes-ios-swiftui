//
//  ActivityIndicatorView.swift
//  EU Votes
//
//  Created by Quentin Biset on 29/06/2020.
//  Copyright © 2020 Quentin Biset. All rights reserved.
//

import SwiftUI

struct ActivityIndicatorView: UIViewRepresentable {
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicatorView>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicatorView>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}

// MARK: -
#if DEBUG
struct ActivityIndicatorView_Previews: PreviewProvider {
    static var previews: some View {
      VStack {
        ActivityIndicatorView(isAnimating: .constant(true), style: .large)
      }
    }
}
#endif

//
//  Assemblies.swift
//  EU Votes
//
//  Created by Quentin Biset on 09/02/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import Foundation
import Swinject
import ProposalsDataRepositories
import UseCases

class UseCasesAssembly: Assembly {
    func assemble(container: Container) {
        container.register(ProposalsSplitViewUseCase.self) { _ in
            let repo = ProposalsDataRepositories.ProposalsRepository()
            return ProposalsSplitViewUseCase(repository: repo)
        }.inObjectScope(.graph)
    }
}

class ViewModelsAssembly: Assembly {
    func assemble(container: Container) {
        container.register(SplitViewModel.self) { resolver in
            let useCase: ProposalsSplitViewUseCase = resolver.resolve(ProposalsSplitViewUseCase.self)!
            return SplitViewModel(useCase: useCase)
        }.inObjectScope(.transient)
    }
}

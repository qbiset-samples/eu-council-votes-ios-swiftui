//
//  Injector.swift
//  EU Votes
//
//  Created by Quentin Biset on 09/02/2024.
//  Copyright © 2024 Quentin Biset. All rights reserved.
//

import Foundation
import Swinject
import UseCases

class Injector {
    // MARK: Properties
    private let assembler = Assembler([UseCasesAssembly(), ViewModelsAssembly()])

    // MARK: Convenience getters
    func getSplitViewModel() -> SplitViewModel {
        return assembler.resolver.resolve(SplitViewModel.self)!
    }
}
